package com.example.calcbayart;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button raz = (Button) findViewById(R.id.raz);
        raz.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EditText value1 = findViewById(R.id.value1);
                EditText value2 = findViewById(R.id.value2);
                TextView result = findViewById(R.id.result);

                value1.setText("");
                value2.setText("");
                result.setText("Résultat");
            }
        });

        final Button equal = (Button) findViewById(R.id.equal);
        equal.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EditText value1 = findViewById(R.id.value1);
                EditText value2 = findViewById(R.id.value2);
                TextView res = findViewById(R.id.result);

                if(value1.getText() != null && value2.getText() != null) {
                    double v1 = Double.parseDouble(value1.getText().toString());
                    double v2 = Double.parseDouble(value2.getText().toString());

                    Double result = calculate(v1, v2);

                    if(result != null) {
                        res.setText(Double.toString(result));
                    } else {
                        res.setText("Calcul impossible");
                    }
                } else {
                    res.setText("Aucune valeur sasie");
                }
            }
        });

        final Button quit = (Button) findViewById(R.id.quit);
        quit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                System.exit(0);
            }
        });
    }

    public Double calculate(double value1, double value2) {
        RadioButton addition = findViewById(R.id.addition);
        RadioButton soustraction = findViewById(R.id.soustraction);
        RadioButton multiplication= findViewById(R.id.multiplication);
        RadioButton division  = findViewById(R.id.division);

        Double result = null;

        if(addition.isChecked()) {
            result = value1 + value2;
        } else if(soustraction.isChecked()) {
            result = value1 - value2;
        } else if(multiplication.isChecked()) {
            result = value1 * value2;
        } else if(division.isChecked() && value2 != 0) {
            result = value1 / value2;
        }

        return result;
    }
}